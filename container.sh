#!/bin/bash

chmount() {
    for fs in "proc:proc" "sysfs:sys" "devtmpfs:dev" "devpts:dev/pts" "tmpfs:dev/shm" "tmpfs:run" "tmpfs:tmp"; do
        fstype=$(echo "${fs}" | cut -d':' -f1)
        path=$(echo "${fs}" | cut -d':' -f2-)
        sudo mount -t "${fstype}" "${fstype}" "${dirpath}/${path}"
    done
}

chumount() {
    for path in "tmp" "run" "dev/shm" "dev/pts" "dev" "sys" "proc"; do
        sudo umount "${dirpath}/${path}"
    done
}

setup_network() {
    osid=$(sudo chroot "${dirpath}" cat /etc/os-release | grep '^ID=' | cut -d'=' -f2-)
    [[ -z "${osid}" ]] && { echo "failed to retrive osid" 1>&2; return 1; }
    case "${osid}" in
        "fedora" | "centos" ) installer="dnf"; package="systemd-container openssh-server iproute iputils tcpdump"; sshservice="sshd";;
        "ubuntu" | "debian" ) installer="apt"; package="systemd ssh iproute2 iputils-ping tcpdump"; sshservice="ssh";;
    esac
    echo "nameserver 1.1.1.1" | sudo tee "${dirpath}/etc/resolv.conf" >/dev/null || { echo "cannot change dns" 1>&2; return 1; }
    chmount
    sudo chroot "${dirpath}" ${installer} update -y || { echo "failed to sync repos" 1>&2; }
    sudo chroot "${dirpath}" ${installer} install -y ${package} || { echo "failed to install ${package}" 1>&2; }
    chumount
    sudo chroot "${dirpath}" systemctl enable systemd-networkd
    sudo chroot "${dirpath}" systemctl enable systemd-resolved
    sudo chroot "${dirpath}" systemctl enable "${sshservice}"
    [[ ! -d "${dirpath}/etc/systemd/network" ]] && mkdir -p "${dirpath}/etc/systemd/network"
    echo -e "[Match]\nName=host0\n[Network]\nDHCP=yes" | sudo tee "${dirpath}/etc/systemd/network/80-container-host0.network" 2>/dev/null || { echo "failed to configure host0 - container network may fail" 1>&2; }
    echo -e "nameserver 127.0.0.1\nnameserver 1.1.1.1" | sudo tee "${dirpath}/etc/resolv.conf" >/dev/null || { echo "cannot point dns to systemd-resolved" 1>&2; }
}

setup_ssh() {
    sshpubkey="${1:?invalid sshpubkey}"
    ak="${dirpath}/root/.ssh/authorized_keys"
    sshdir="$(dirname ${ak})"
    [[ ! -d "${sshdir}" ]] && { sudo mkdir -p "${sshdir}" && { echo "${sshpubkey}" | sudo tee -a "${ak}" >/dev/null; } && sudo chmod 700 "${sshdir}" || { echo "failed to setup ssh" 1>&2; return 1; }; }
}

create() {
    name="${1:?invalid container-name}"
    registry="${2:?invalid refspec}"
    passwd="${3}"
    [[ "${passwd}" == "-" ]] && { echo -n "root password: " && stty -echo && read passwd && stty echo && echo; }
    sshpubkey="${4}"
    dirpath="$(pwd)/.${name}"
    skopeo copy "${registry}" "dir:${dirpath}" || { echo "failed to import ${registry}" 1>&2; return 1; }
    roots=$(jq -r '.layers[].digest' "${dirpath}/manifest.json" | cut -d':' -f2-)
    [[ "${roots}" == "" ]] && { echo "failed to find roots" 1>&2; return 1; }
    for root in ${roots}; do gunzip -dc "${dirpath}/${root}" | sudo tar x -C "${dirpath}" || { echo "failed to extract ${root}" 1>&2; return 1; }; done
    echo "${name}" | sudo tee "${dirpath}/etc/hostname" >/dev/null
    [[ -n "${passwd}" ]] && { echo "root:${passwd}" | sudo chpasswd --root "${dirpath}"; }
    [[ -n "${sshpubkey}" ]] && setup_network && setup_ssh "${sshpubkey}" || { echo "network setup failed - ssh login will fail" 1>&2; }
    sudo machinectl import-fs "${dirpath}" "${name}" || { echo "failed to import rootfs to system" 1>&2; return 1; }
    sudo rm -fr "${dirpath}"
}

remove() {
    name="${1:?invalid name}"
    sudo machinectl remove "${name}"
}

start() {
    name="${1?invalid name}"
    { sudo machinectl list | grep "${name}" >/dev/null; } && return 0
	if [[ ! -e /sys/class/net/bridge0 ]]; then
		sudo ip link add bridge0 type bridge
		sudo ip addr add 192.168.6.1/24 dev bridge0
		sudo ip link set dev bridge0 up
		sudo sysctl net.ipv4.ip_forward=1
		sudo iptables -t nat -A POSTROUTING -s 192.168.6.0/24 -j MASQUERADE
	fi
    sudo systemd-nspawn --quiet --boot --link-journal=try-guest --network-bridge=bridge0 --bind $(pwd):/hostpwd  --console=passive --machine="${name}" &
}

run() {
    name="${1?invalid name}"
    start "${name}"
    sleep 2
    machinectl login "${name}"
}

stop() {
    name="${1?invalid name}"
    sudo machinectl stop "${name}"
}

if [[ "${1}" == "--help" ]] || [[ "${1}" == "-h" ]]; then
    echo "${0} create <container-name> <skopeo-refspec> [rootpassword|-] [sshpubkey]"
    echo "${0} remove <container-name>"
    echo "${0} start <container-name>"
    echo "${0} run <container-name>"
    echo "${0} stop <container-name>"
    exit 0
fi

"${@}"
