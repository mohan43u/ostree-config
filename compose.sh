if [[ "${1}" == "--help" ]] || [[ "${1}" == "-h" ]]; then
	echo "${0} <srv> <rpm-ostree-treefile>"
	exit 0
fi
base="${1:?no base provided}"
treefile="${2:?no treefile provided}"

if [[ -z "$(which rpm-ostree)" ]]; then
	dnf install -y rpm-ostree || { echo "failed to install rpm-ostree" 1>&2; exit 1; }
fi

if [[ ! -d "${base}" ]]; then
	mkdir -p "${base}"/{repo,cache,work}
	ostree init --repo "${base}"/repo --mode archive
fi
#mount -t tmpfs tmpfs "${base}"/work || { echo "failed to mount work" 1>&2; exit 1; }
rpm-ostree compose tree --repo "${base}"/repo --cachedir "${base}"/cache --workdir "${base}"/work "${treefile}"
ostree summary -u --repo "${base}"/repo
#umount "${base}"/work
