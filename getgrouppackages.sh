if [[ "${1}" == "--help" ]] || [[ "${1}" == "-h" ]]; then
    echo "${0} <base> <group-name> [group-name ...]"
    exit 0
fi
base="${1:?invalid base}"
shift
rm -fr "${base}/dnfroot"
rm -fr "${base}/pkgs"
dnf group install -y --installroot "${base}/dnfroot" --downloadonly --downloaddir "${base}/pkgs" "${@}"
rpm --query --queryformat '%{name}\n' "${base}/pkgs"/* | sed 's/^/"/g;s/$/",/g;1s/^/[\n/g;$s/$/\n]/g'
rm -fr "${base}/pkgs"
rm -fr "${base}/dnfroot"
