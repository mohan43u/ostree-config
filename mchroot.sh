chmount() {
        for fs in "proc:proc" "sysfs:sys" "devtmpfs:dev" "devpts:dev/pts" "tmpfs:dev/shm" "tmpfs:run" "tmpfs:tmp"; do
                fstype=$(echo "${fs}" | cut -d':' -f1)
                path=$(echo "${fs}" | cut -d':' -f2-)
                sudo mount -t "${fstype}" "${fstype}" "${dirpath}/${path}"
        done
}

chumount() {
        for path in "tmp" "run" "dev/shm" "dev/pts" "dev" "sys" "proc"; do
                sudo umount "${dirpath}/${path}"
        done
}

dirpath="${1}"
shift
chmount
sudo chroot "${dirpath}" "${@}"
chumount
